formData = new FormData();

$("#formRegistrar").on("submit",function(e){
    e.preventDefault();
    
    let nome = $("#nome").val();
    nome = nome[0].toUpperCase() + nome.slice(1).toLowerCase();
    let sobrenome = $("#sobrenome").val();
    sobrenome = sobrenome[0].toUpperCase() + sobrenome.slice(1).toLowerCase();
    let email = $("#email").val();
    email = email.toLowerCase();
    let senha1 = $("#senha1").val();
    let senha2 = $("#senha2").val();

    if(senha1 == senha2){
        formData.append("nome",nome);
        formData.append("sobrenome",sobrenome);
        formData.append("email",email);
        formData.append("senha",senha1);

        let ajax = new XMLHttpRequest();
        ajax.onreadystatechange = function(){
            if(ajax.readyState == 4){
                if(ajax.responseText == "Usuário inserido com sucesso!"){
                    //Exibe um alerta de sucesso
                    toastr["success"]("Usuário cadastrado com sucesso")
                    toastr.options = {
                      "closeButton": true,
                      "debug": false,
                      "newestOnTop": false,
                      "progressBar": false,
                      "positionClass": "toast-top-right",
                      "preventDuplicates": true,
                      "onclick": null,
                      "showDuration": "1000",
                      "hideDuration": "1000",
                      "timeOut": "2000",
                      "extendedTimeOut": "1000",
                      "showEasing": "swing",
                      "hideEasing": "linear",
                      "showMethod": "fadeIn",
                      "hideMethod": "fadeOut"
                    }
                    //Apaga os dados do formulário
                    $("#formRegistrar").trigger("reset");
                }else{
                    //Exibe um alerta de erro
                    toastr["error"](ajax.responseText);
                    toastr.options = {
                      "closeButton": true,
                      "debug": false,
                      "newestOnTop": false,
                      "progressBar": false,
                      "positionClass": "toast-top-right",
                      "preventDuplicates": true,
                      "onclick": null,
                      "showDuration": "1000",
                      "hideDuration": "1000",
                      "timeOut": "2000",
                      "extendedTimeOut": "1000",
                      "showEasing": "swing",
                      "hideEasing": "linear",
                      "showMethod": "fadeIn",
                      "hideMethod": "fadeOut"
                    }
                }
            }
        }
        
        ajax.open("POST","../Controle/Registrar/RegistrarUsuario.php");
        ajax.send(formData);
    }else{
        toastr["warning"]("As senhas digitadas são diferentes!");
        toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-top-right",
        "preventDuplicates": true,
        "onclick": null,
        "showDuration": "1000",
        "hideDuration": "1000",
        "timeOut": "2000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
        }
    }
});

$("#formEntrar").on("submit",function(e){
    e.preventDefault();

    let email = $("#emailEntrar").val();
    email = email.toLowerCase();
    let senha = $("#senhaEntrar").val();

    formData.append("email",email);
    formData.append("senha",senha);

    let ajax = new XMLHttpRequest();
    ajax.onreadystatechange = function(){
        if(ajax.readyState == 4){
            if(ajax.responseText == "Email não cadastrado!"){
                //Exibe um alerta de erro
                toastr["error"](ajax.responseText);
                toastr.options = {
                  "closeButton": true,
                  "debug": false,
                  "newestOnTop": false,
                  "progressBar": false,
                  "positionClass": "toast-top-right",
                  "preventDuplicates": true,
                  "onclick": null,
                  "showDuration": "1000",
                  "hideDuration": "1000",
                  "timeOut": "2000",
                  "extendedTimeOut": "1000",
                  "showEasing": "swing",
                  "hideEasing": "linear",
                  "showMethod": "fadeIn",
                  "hideMethod": "fadeOut"
                }
            }else{
                if(ajax.responseText == "Senha incorreta!"){
                    toastr["error"](ajax.responseText);
                    toastr.options = {
                      "closeButton": true,
                      "debug": false,
                      "newestOnTop": false,
                      "progressBar": false,
                      "positionClass": "toast-top-right",
                      "preventDuplicates": true,
                      "onclick": null,
                      "showDuration": "1000",
                      "hideDuration": "1000",
                      "timeOut": "2000",
                      "extendedTimeOut": "1000",
                      "showEasing": "swing",
                      "hideEasing": "linear",
                      "showMethod": "fadeIn",
                      "hideMethod": "fadeOut"
                    }      
                }else{
                    //Exibe um alerta de sucesso
                    window.location.href = "../";
                }
                
            }
        }
    }
    
    ajax.open("POST","../Controle/Entrar/Entrar.php");
    ajax.send(formData);
});