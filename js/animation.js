let widthTela = $(window).width();
let logo = document.getElementById("logo");
let width = $("#divAnimation").width();

width = width-100;
width = width.toString()+"px";
logo.style.setProperty("--moveEnd",width);

//Aplica a animação no #logo, modificando a string abaixo no arquivo "animation.css"
logo.style.animation = "rotate 1.9s ease-in-out 0s infinite alternate";   

//Quando o width da tela for modificado, ele atualiza o width do #divAnimation
$(window).resize(function(){
    let widthTela = $(window).width();
    if(widthTela <= 991){
        let width = $("#divAnimation").width();
        width = width-100;
        width = width.toString()+"px";
        logo.style.setProperty("--moveEnd",width);
        logo.style.animation = "rotate 1.9s ease-in-out 0s infinite alternate";
    }else{
        logo.style.animation = "rotate 0s";
    }
});