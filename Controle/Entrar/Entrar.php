<?php
session_start();
require_once("EntrarControle.php");

$controle = new EntrarControle();
$email = $_POST["email"];
$senha = $_POST["senha"];

$usuario = $controle->selecionarUm($email);
if($usuario == null){
    echo "Email não cadastrado!";
}else{
    if(password_verify($senha,$usuario[0]->getSenha())){
        //Só aqui que a sessão será criada e o usuário será redirecionado para a home da aplicação
        $_SESSION["nome"] = $usuario[0]->getNome();
        $_SESSION["logged"] = true;
        echo "Seja bem vindo, ".$_SESSION["nome"]."<br>";
    }else{
        echo "Senha incorreta!";
    }
}
?>