<?php
require_once("../Conexao/Conexao.php");
require_once("../../Modelo/Usuario.php");
class EntrarControle{
	public function selecionarUm($email){
		try{
			$conexao = new Conexao("../Conexao/banco.ini");
			$comando = $conexao->getPDO()->prepare("SELECT * FROM usuario WHERE email=:e;");
			$comando->bindParam("e", $email);
			if($comando->execute()){
				$usuario = $comando->fetchAll(PDO::FETCH_CLASS, "Usuario");
			}
		}catch(PDOException $e){
			echo "Erro no banco: {$e->getMessage()}";
		}catch(Exception $e){
			echo "Erro geral: {$e->getMessage()}";
		}finally{
			$conexao->fecharConexao();
			return $usuario;
		}
	}
}
?>