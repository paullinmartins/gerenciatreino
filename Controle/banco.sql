DROP SCHEMA IF EXISTS banco;
CREATE SCHEMA IF NOT EXISTS banco;

CREATE TABLE banco.usuario(
    email varchar(30) NOT NULL,
    senha VARCHAR(60) NOT NULL,
    nome VARCHAR(50) NOT NULL,
    sobrenome VARCHAR(50) NOT NULL,
    PRIMARY KEY (email)
)