<?php
require_once("Registrar.php");
require_once("../../Modelo/Usuario.php");

$modelo = new Usuario();
$modelo->setNome($_POST["nome"]);
$modelo->setSobrenome($_POST["sobrenome"]);
$modelo->setEmail($_POST["email"]);
$modelo->setSenha(password_hash($_POST["senha"], PASSWORD_DEFAULT));


$controle = new Registrar();
if($controle->inserirUsuario($modelo)){
    echo "Usuário inserido com sucesso!";
}
?>