<?php
require_once("../Conexao/Conexao.php");
require_once("../../Modelo/Usuario.php");
class Registrar{
	public function inserirUsuario($usuario){
		try{
			$resultado = false;
			$conexao = new Conexao("../Conexao/banco.ini");
			$comando = $conexao->getPDO()->prepare("INSERT INTO usuario (email,senha,nome,sobrenome) VALUES (:e, :se, :n, :so);");
			$email = $usuario->getEmail();
			$senha = $usuario->getSenha();
            $nome = $usuario->getNome();
            $sobrenome = $usuario->getSobrenome();
			$comando->bindParam("e",$email);
			$comando->bindParam("se",$senha);
            $comando->bindParam("n",$nome);
			$comando->bindParam("so",$sobrenome);
			if($comando->execute()){
				$resultado = true;
			}
		}catch(PDOException $e){
			echo "Usuário já cadastrado";
		}catch(Exception $e){
			echo "Erro geral: {$e->getMessage()}";
		}finally{
			$conexao->fecharConexao();
			return $resultado;
		}
	}
}
?>