<?php
echo "
<!DOCTYPE html>
<html>
<head>
    <meta charset='UTF-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1.0'>

    <title>Gerenciatreino - Gerencie seu treino da melhor forma</title>
    <meta name='description' content='O melhor gerenciador para seu treino se encontra aqui. Com ele você consegue manter total controle dos seus treinos de forma prática e rápida, comparar suas fotos de antes e depois, calcular e atualizar a sua taxa metabólica basal e muito mais em um só lugar.' />

    <meta name='keywords' contente='Gerenciatreino, Treino, Gerenciar'>

    <link href='css/bootstrap.css' rel='stylesheet'>
    <link href='css/index.css' rel='stylesheet'>
    <link href='css/animation.css' rel='stylesheet'>
    <link href='css/media.css' rel='stylesheet'>
    <link href='https://cdn.jsdelivr.net/npm/boxicons@latest/css/boxicons.min.css' rel='stylesheet'>
    
    <!-- Icon -->
    <link rel='shortcut icon' type='imagem/png' href='img/Favicon.png'/>
</head>
<body id='body-pd'>";

//Parallax
echo '
<!--Navbar-->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
        <div class="container-fluid">
            <a href="#"><img src="img/imgNav.png" width="180px" style="margin-right: 14px;"></a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarColor01"  aria-label="Toggle">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="navbar-collapse collapse" id="navbarColor01">
                <ul class="navbar-nav mb-2 mb-md-0 ms-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="Entrar/">Entrar</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="Registrar/">Registrar</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="Contato/">Contato</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="Sobre/">Sobre</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <div class="container-fluid containerParallax d-flex flex-column justify-content-center align-items-center">
        <p class="tituloParallax">-GERENCIATREINO-</p>
        <span class="caixaTxtParallax"><p class="txtParallax">Gerencie seu treino da melhor forma</p></span>
    </div>
';

//Caixa de boas vindas
echo '
    <div class="row my-5" style="margin-right: 0px;">
        <div class="col-md-6" id="divAnimation">
            <img class="mx-auto d-block img-fluid" id="imagemUm" src="img/prancheta.png" width="250px">
            <img id="logo" src="img/Logo.png">
        </div>
        <div class="col-md-6" style="border-left: solid; border-width: 1px;">
            <div class="row">
                <h1 class="mx-3 mt-3">O que é o gerenciatreino?</h1>
                <div class="col-xl-6 col-lg-12">
                    <p class="mx-3 mt-3" >O melhor gerenciador para seu treino se encontra aqui. Com ele você consegue manter total controle dos seus treinos de forma prática e rápida. </br> </br> Com ele você pode comparar suas fotos de antes e depois, calcular e atualizar a sua taxa metabólica basal, armazenar dados importantes para uso posterior e muito mais.</p>
                </div>
            </div>
        </div>
    </div>
';

//Funcionalidades
echo '
    <div class="container-fluid mx-0 pt-5" style="background-color: #000;">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-4 col-md-6 mb-5">
                    <div class="d-flex justify-content-center">
                        <img class="rounded-circle mx-0 my-0 circleBase" src="img/armazenamento.png">
                    </div>
                    <div class="d-flex justify-content-center mt-5">
                        <h1 class="txt1 text-justify text-center tf mb-3">ARMAZENAMENTO</h1>
                    </div>
                    <div class="d-flex justify-content-center divFuncionalidades">
                        <p class="mt-3 tf2" style="font-size: 22px;  color: #fff;">Armazene suas fotos e vídeos e compare seu antes e depois com nosso serviço inteligente</p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 mb-5">
                    <div class="d-flex justify-content-center">
                        <img class="rounded-circle mx-0 my-0 circleBase" src="img/calculadora.png">
                    </div>
                    <div class="d-flex justify-content-center mt-5">
                        <h1 class="txt1 text-justify text-center tf mb-3">CALCULADORA</h1>
                    </div>
                    <div class="d-flex justify-content-center divFuncionalidades">
                        <p class="mt-3 tf2" style="font-size: 22px; color: #fff">Use nossa calculadora para saber sua taxa metabólica basal, IMC e muito mais em um só lugar</p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-12 mb-5" id="divDicas">
                    <div class="d-flex justify-content-center">
                        <img class="rounded-circle mx-0 my-0 circleBase" src="img/dicas.png">
                    </div>
                    <div class="d-flex justify-content-center mt-5">
                        <h1 class="txt1 text-justify text-center tf mb-3">DICAS</h1>
                    </div>
                    <div class="d-flex justify-content-center divFuncionalidades">
                        <p class="mt-3 tf2" style="font-size: 22px;  color: #fff;">Nossos serviços indicam treinos, dietas e muito mais baseado nos dados informados por você</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
';

//Classificação
echo '
    <div class="container-fluid my-5">
        <h1 class="txt2 text-center" style="font-family: Arial, Helvetica, sans-serif;">CLASSIFICAÇÃO</h1>
        <hr class="mt-5">
        <div class="row mt-5">
            <div class="col-lg-3 col-md-2 col-sm-1"></div>
            <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12 d-flex justify-content-center">
                <h2 class="txt2" style="font-weight: normal;">Serviços</h2>
            </div>
            <div class="col-lg-3 col-md-5 col-sm-7 d-flex justify-content-center">
                <p class="txt3 mt-2 mx-2">0%</p><span class="progressBar1"></span><p class="txt3 mt-2">100%</p>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-1"></div>
        </div>

        <div class="row mt-5">
            <div class="col-lg-3 col-md-2 col-sm-1"></div>
            <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12 d-flex justify-content-center">
                <h2 class="txt2" style="font-weight: normal;">Eficácia</h2>
            </div>
            <div class="col-lg-3 col-md-5 col-sm-7 d-flex justify-content-center">
                <p class="txt3 mt-2 mx-2">0%</p><span class="progressBar1"></span><p class="txt3 mt-2">100%</p>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-1"></div>
        </div>

        <div class="row mt-5">
            <div class="col-lg-3 col-md-2 col-sm-1"></div>
            <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12 d-flex justify-content-center">
                <h2 class="txt2" style="font-weight: normal;">Qualidade</h2>
            </div>
            <div class="col-lg-3 col-md-5 col-sm-7 d-flex justify-content-center">
                <p class="txt3 mt-2 mx-2">0%</p><span class="progressBar1"></span><p class="txt3 mt-2 mx">100%</p>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-1"></div>
        </div>
    </div>
';

//Caixa de boas vindas
echo '
    <footer class="text-center text-lg-start bg-light text-muted">
    
        <section class="d-flex justify-content-center justify-content-lg-between border-bottom border-top mt-5">
            <div class="container text-center text-md-start mt-5">
                <div class="row">
                    <div class="col-md-3 col-lg-4 col-xl-3 mx-auto mb-4">
                        <h6 class="text-uppercase fw-bold mb-4">
                            Gerenciatreino
                        </h6>
                        <p>Obrigado por escolher a gente! Nos acompanhe através das nossas redes sociais.</p>
                    </div>

                    <div class="col-md-4 col-lg-3 col-xl-3 mx-auto mb-md-0 mb-4">
                        <h6 class="text-uppercase fw-bold mb-4">Contato</h6>
                        <p>@gerenciatreino</p>
                        <p>gerenciatreino@gmail.com</p>
                    </div>                    
                </div>
            </div>
        </section>

        <div class="text-center p-4" style="background-color: rgba(0, 0, 0, 0.05);">
            © 2021 Copyright:
            <a class="text-reset fw-bold" href="http://gerenciatreino.ga/">gerenciatreino.ga</a>
        </div>
    </footer>
';

echo "
    <script src='js/jquery.js'></script>
    <script src='js/bootstrap.js'></script>
    <script src='js/animation.js'></script>
    <script src='js/home.js'></script>
</body>
</html>
";
?>