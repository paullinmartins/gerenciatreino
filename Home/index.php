<?php
session_start();
$logged = $_SESSION["logged"] ?? null;
if($logged == true){
    //Caso o valor "logged" seja true, ele vai chamar a classe home
    require_once("home.php");
}else{
    //Caso o valor "logged" seja falso, ele vai chamar homeInformation
    require_once("homeInformation.php");
}
?>