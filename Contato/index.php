<?php
echo '
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Gerenciatreino - Contato</title>
    <link href="../css/bootstrap.css" rel="stylesheet">
    <link href="../css/Contato.css" rel="stylesheet">
    <link rel="icon" type="imagem/png" href="../img/Favicon.png"/>
</head>
<body>

    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
        <div class="container-fluid">
            <a href="../"><img src="../img/imgNav.png" width="180px" style="margin-right: 14px;"></a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarColor01"  aria-label="Toggle">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="navbar-collapse collapse" id="navbarColor01">
                <ul class="navbar-nav mb-2 mb-md-0 ms-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="../Entrar/">Entrar</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="../Registrar/">Registrar</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" href="../Contato/">Contato</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="../Sobre">Sobre</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>


    <div class="container-float parallaxRegistrar d-flex flex-column justify-content-center align-items-center">
        <div class="form-cadastro">
            <h1 class="mt-3" id="txtCadastro" style="margin-bottom: 35px;">Contato</h1>
            <form action="#" method="post">
            
                <div class="row">
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <input type="text" class="form-control" placeholder="Nome:" aria-label="First name">
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <input type="text" class="form-control" placeholder="Sobrenome: " aria-label="Last name">
                    </div>
                </div>

                <div class="form-group">
                    <input type="email" id="email" name="email" class="form-control" placeholder="Email:" required />
                </div>
                <div class="form-group mt-2">
                    <textarea type="password" class="form-control" id="msg" name="msg" placeholder="Mensagem:" required rows="5"></textarea>
                </div>

                <div class="form-group d-flex justify-content-center mt-4">
                    <button class="w-40 btn btn-outline-dark" type="submit" style="border-radius: 15px; width: 110px;">Enviar</button>
                </div>
            </form>
            <hr class="mt-4">
            <div class="row">
                <div class="col-lg-12 mt-2 mb-3 d-flex justify-content-center">
                    <a href="https://www.instagram.com/gerenciatreino/" target="_blank"><img src="../img/iconInstagram.png" alt="instagram" width="25px" height="25px" class="mx-3"></a>
                    <a href="https://twitter.com/gerenciatreino" target="_blank"><img src="../img/iconTwitter.png" alt="twitter" width="25px" height="25px" class="mx-3"></a>
                    <img src="../img/iconGmail.png" href="#" alt="gmail" id="gmail" width="30px" height="25px" class="mx-3">
                </div>
            </div>
        </div>
    </div>

    <script src="../js/jquery.js"></script>
    <script src="../js/bootstrap.js"></script>
    <script src="../js/app.js"></script>
</body>
</html>
';
?>
