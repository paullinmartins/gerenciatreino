<?php
echo '
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Gerenciatreino - Entrar</title>
    <link href="../css/bootstrap.css" rel="stylesheet">
    <link href="../css/Entrar.css" rel="stylesheet">
    <link href="../css/toastr.min.css" rel="stylesheet">
    <link href="../css/media.css" rel="stylesheet">
    <link rel="icon" type="imagem/png" href="../img/Favicon.png"/>
</head>
<body>

    <!--Navbar-->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
        <div class="container-fluid">
            <a href="../"><img src="../img/imgNav.png" width="180px" style="margin-right: 14px;"></a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarColor01"  aria-label="Toggle">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="navbar-collapse collapse" id="navbarColor01">
                <ul class="navbar-nav mb-2 mb-md-0 ms-auto">
                    <li class="nav-item">
                        <a class="nav-link active" href="../Entrar/">Entrar</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="../Registrar/">Registrar</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="../Contato/">Contato</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="../Sobre">Sobre</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <div class="container-float parallaxEntrar d-flex flex-column justify-content-center align-items-center">
        <div class="form-cadastro">
            <h1 class="mt-3 mb-4" id="txtCadastro">Entrar</h1>
            <form id="formEntrar">
                <div class="form-group">
                    <input type="email" class="form-control" placeholder="Email:" required name="emailEntrar" id="emailEntrar" />
                </div>
                <div style="position: relative;">
                        <input type="password" class="form-control" placeholder="Senha:" required="on" minlength="8" name="senhaEntrar" id="senhaEntrar"/>
                        <img src="../img/eyeIconClose.png" id="eye" width="30px">
                    </div>
                <div class="form-group d-flex justify-content-center mt-3">
                    <button class="w-40 btn btn-outline-dark" type="submit" style="border-radius: 15px; width: 110px;">Entrar</button>
                </div>
            </form>
            <p class="text-center mt-4">Ainda não tem uma conta? <a href="../Registrar/" id="linkRedefinir">Clique aqui</a> para se registrar.</p>
        </div>
    </div>

    <script src="../js/jquery.js"></script>
    <script src="../js/bootstrap.js"></script>
    <script src="../js/toastr.min.js"></script>
    <script src="../js/Ajax.js"></script>
    <script src="../js/app.js"></script>
</body>
</html>
';
?>