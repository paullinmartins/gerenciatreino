<?php
session_start();
if ($_SESSION["logged"] != true) die(header("Location: ../"));

echo'
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Gerenciatreino - Gerencie seu treino da melhor forma</title>
    <meta name="description" content="O melhor gerenciador para seu treino se encontra aqui. Com ele você consegue manter total controle dos seus treinos de forma prática e rápida, comparar suas fotos de antes e depois, calcular e atualizar a sua taxa metabólica basal e muito mais em um só lugar." />

    <meta name="keywords" contente="Gerenciatreino, Treino, Gerenciar">

    <!--Stylesheets-->
    <link href="../css/bootstrap.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/boxicons@latest/css/boxicons.min.css" rel="stylesheet">
    
    <link href="../css/home.css" rel="stylesheet">
    
    
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/@mdi/font@6.5.95/css/materialdesignicons.min.css">

    <!--Scripts-->
    <script src="../js/jquery.js"></script>    
    <script src="../js/popper.min.js"></script>
    <script src="../js/bootstrap.js"></script>    
    
    <!-- Icon -->
    <link rel="shortcut icon" type="imagem/png" href="img/Favicon.png"/>
</head>

<body id="body-pd">

    <header class="header" id="header">
        <div class="header_toggle"> <i class="bx bx-menu" id="header-toggle"></i> </div>
        
        <!--Profile-->
        <div class="nav-item dropdown" style="margin-right: -15px;">
            <a class="nav-link pe-0" id="navbarDropdownUser" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="padding: 0px 0px 0px 0px; margin: 0px 0px 0px 0px; width: 50px;">
                <div class="avatar avatar-xl">
                    <div class="header_img"> <img src="../img/iconUserNone.png" alt=""></div>
                </div>
            </a>
            <div class="dropdown-menu dropdown-menu-end py-0" aria-labelledby="navbarDropdownUser">
                <div class="bg-white dark__bg-1000 rounded-2 py-2">
                    <p class="dropdown-item fw-bold text-dark my-0">'.$_SESSION["nome"].'</p>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#!">Configurações</a>
                    <a class="dropdown-item" href="#!">Sobre</a>
                    <a class="dropdown-item" href="#!">Contato</a>
                    <a class="dropdown-item" href="#!">Ajuda</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="../Controle/logout/logout.php">Sair</a>
                </div>
            </div>
        </div>
    </header>

    <div class="l-navbar" id="nav-bar">
        <nav class="nav">
            <div> 
                <a href="../" class="nav_logo" style="margin-top: -9px;">
                    <img src="../img/Favicon.png" width="30px">
                    <img src="../img/imgNav.png" width="135px">
                </a>
                <div class="dropdown-divider dd"></div>
                <div class="nav_list">
                    <a href="../" class="nav_link"> <img src="../img/iconHome.png" width="22px" title="Home"> <span class="nav_name">Home</span> </a>
                    <a href="../tmb" class="nav_link"> <img src="../img/iconTmb.png" width="22px" title="Taxa metabólica basal"> <span class="nav_name">Taxa metabólica</span> </a>
                    <a class="nav_link active"> <img src="../img/iconIMC.png" width="20px" title="IMC"> <span class="nav_name">IMC</span> </a>
                    <a href="../fotos" class="nav_link"> <img src="../img/iconFotos.png" width="20px" title="Fotos"> <span class="nav_name">Fotos</span> </a>
                </div>
            </div>
        </nav>
    </div>


    <div class="container-fluid">
        <h1 class="text-center" style="margin-top: 120px; margin-bottom: 50px;" >IMC</h1>    
    </div>

    <script src="../js/home.js"></script>  
</body>
</html>
';
?>