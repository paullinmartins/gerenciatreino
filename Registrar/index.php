<?php
echo '
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Gerenciatreino - Registrar</title>
    <link href="../css/bootstrap.css" rel="stylesheet">
    <link href="../css/Registrar.css" rel="stylesheet">
    <link href="../css/toastr.min.css" rel="stylesheet">
    <link href="../css/media.css" rel="stylesheet">
    <link rel="icon" type="imagem/png" href="../img/Favicon.png"/>
</head>
<body>
    <!--navbar-->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
        <div class="container-fluid">
            <a href="../"><img src="../img/imgNav.png" width="180px" style="margin-right: 14px;"></a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarColor01"  aria-label="Toggle">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="navbar-collapse collapse" id="navbarColor01">
                <ul class="navbar-nav mb-2 mb-md-0 ms-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="../Entrar/">Entrar</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" href="../Registrar/">Registrar</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="../Contato/">Contato</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="../Sobre">Sobre</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>


    <div class="container-float parallaxRegistrar d-flex flex-column justify-content-center align-items-center">
        <div class="form-cadastro">
            <h1 class="mt-3" id="txtCadastro" style="margin-bottom: 35px;">Registrar</h1>
            <form id="formRegistrar">   
                <div class="row">
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <input type="text" class="form-control" placeholder="Nome:" aria-label="First name" required id="nome" name="nome" pattern="[A-Za-záàâãéèêíïóôõöúçñÁÀÂÃÉÈÍÏÓÔÕÖÚÇÑ ]+$" />
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <input type="text" class="form-control" placeholder="Sobrenome: " aria-label="Last name" required id="sobrenome" name="sobrenome" pattern="[A-Za-záàâãéèêíïóôõöúçñÁÀÂÃÉÈÍÏÓÔÕÖÚÇÑ ]+$" />
                    </div>
                </div>

                <div class="form-group">
                    <input type="email" class="form-control" placeholder="Email:" required id="email" name="email" />
                </div>
                <div class="form-group mt-2">
                    <div style="position: relative;">
                        <input type="password" class="form-control" placeholder="Senha:" required minlength="8" id="senha1" name="senha1"/>
                        <img src="../img/eyeIconClose.png" id="eye1" width="30px">
                    </div>
                </div>
                <div class="form-group mt-2 mb-4">
                    <div style="position: relative;">
                        <input type="password" class="form-control" placeholder="Senha novamente:" required minlength="8" id="senha2"  name="senha2" />
                        <img src="../img/eyeIconClose.png" id="eye2" width="30px">
                    </div>
                </div>

                <div class="form-check form-group d-flex justify-content-center mt-3" style="margin: 0px 0px 30px 0px;">
                    <input class="form-check-input" type="checkbox" style="margin-right: 10px;" required id="check" />
                    <label class="form-check-label" for="check">
                        Ao clicar, você concorda com os <a href="#" id="linkRedefinir">termos</a> do gerenciatreino.
                    </label>
                </div>
                <div class="form-group d-flex justify-content-center mt-3">
                    <button class="w-40 btn btn-outline-dark" type="submit" style="border-radius: 15px; width: 110px;">Registrar</button>
                </div>
            </form>
            <p class="text-center mt-4">Já tem uma conta? <a href="../Entrar/" id="linkRedefinir">Clique aqui</a> para entrar.</p>
        </div>
    </div>

    <script src="../js/jquery.js"></script>
    <script src="../js/bootstrap.js"></script>
    <script src="../js/toastr.min.js"></script>
    <script src="../js/Ajax.js"></script>
    <script src="../js/app.js"></script>
</body>
</html>
';
?>
