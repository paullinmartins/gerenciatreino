<?php

echo '
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Gerenciatreino - Sobre</title>
    <link href="../css/bootstrap.css" rel="stylesheet">
    <link href="../css/Sobre.css" rel="stylesheet">
    <link rel="icon" type="imagem/png" href="../img/Favicon.png"/>
</head>
<body>

    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
        <div class="container-fluid">
            <a href="../"><img src="../img/imgNav.png" width="180px" style="margin-right: 14px;"></a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarColor01"  aria-label="Toggle">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="navbar-collapse collapse" id="navbarColor01">
                <ul class="navbar-nav mb-2 mb-md-0 ms-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="../Entrar/">Entrar</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="../Registrar/">Registrar</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="../Contato/">Contato</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" href="../Sobre/">Sobre</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <div class="container-float background d-flex flex-column justify-content-center align-items-center">
        <h1>Em desenvolvimento</h1>
    </div>

    <script src="../js/jquery.js"></script>
    <script src="../js/bootstrap.js"></script>
    <script src="../js/app.js"></script>
</body>
</html>
';

/*

    <div class="container-float background d-flex flex-column justify-content-center align-items-center">
        <div class="caixa">
            <h1 id="txt1" class="my-4">SOBRE</h1>

            <p><span style="font-weight: bold;">Objetivo do gerenciatreino: </span> nosso objetivo é facilitar o gerenciamento
            do seu treino, possibilitando o armazenamento de todas as informações em um único lugar, nossa plataforma. Com ela
            é possível:</p>

            <ul>
                <li class="my-1"><span style="font-weight: bold;">Armazenar fotos: </span>ao armazenar suas fotos aqui, você
                conseguirá acompanhar o seu rendimento desde o início dos treinos até agora.</li>
                <li class="my-1"><span style="font-weight: bold;">Acompanhar sua nutrição: </span>com a nossa calculadora
                de taxa metabólica basal, você pode acompanhar quantas calorias você deve consumir por dia</li>
                <li class="my-1"><span style="font-weight: bold;">Calcular seu IMC: </span>não é de hoje que muitos dizem
                que o IMC não é 100% confiável, mas alguns especialistas ainda usam, por isso não podíamos deixar de colocar.</li>
            </ul>

            <p>A plataforma possui diversas funções. Sobre elas há um ícone de perguntas
            <img id="iconPergunta" class="mx-1" src="../img/iconPergunta.png" alt="(ícone de perguntas)" width="20px" height="20px">
            que ao ser clicado ele mostra informações sobre aquela função.</p>
        </div>
    </div>
*/
?>