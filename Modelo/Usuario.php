<?php
class Usuario{
    private $email;
    private $senha;
    private $nome;
    private $sobrenome;
    public function setEmail($em){
        $this->email = $em;
    }
    public function getEmail(){
        return $this->email;
    }
    public function setSenha($se){
        $this->senha = $se;
    }
    public function getSenha(){
        return $this->senha;
    }
    public function setNome($no){
        $this->nome = $no;
    }
    public function getNome(){
        return $this->nome;
    }
    public function setSobrenome($so){
        $this->sobrenome = $so;
    }
    public function getSobrenome(){
        return $this->sobrenome;
    }
}
?>